﻿using LuandryCustomerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LuandryCustomerApp.Controllers
{
    public class CustomersController : Controller
    {
        private ModelMapping modelMapping = new ModelMapping();
        private lddsDBEntities dbContext = new lddsDBEntities();
        // GET: Customers
        public ActionResult Index(string sortField = "id_asc", int pageNumber = 1, int pageSize = 10)
        {
            IQueryable<customer> query0 = dbContext.customers;
            var query = query0.Select(x => new PlacedOrderListViewModel()
            {
                id = x.id,
                created_at = x.created_at,
                updated_at = x.updated_at,
                deleted_at = x.deleted_at,
            });
            return View();
        }
    }
}