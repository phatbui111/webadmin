﻿using LuandryCustomerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LuandryCustomerApp.Controllers
{
    public class ListViewComponentsController : Controller
    {
        [ChildActionOnly]
        public PartialViewResult RenderPaging(PagingViewModel pagingViewModel)
        {
            int pageNumber = pagingViewModel.PageNumber,
                startPage = 1,
                endPage = pagingViewModel.TotalPages;

            if (pageNumber <= (startPage + 2) && endPage > 4)
            {
                endPage = startPage + 4;
            }
            else if (pageNumber >= (endPage - 2) && startPage < (endPage - 4))
            {
                startPage = endPage - 4;
            }
            else if ((pageNumber - startPage) > 2 && (endPage - pageNumber) > 2)
            {
                startPage = pageNumber - 2;
                endPage = pageNumber + 2;
            }

            pagingViewModel.StartPage = startPage;
            pagingViewModel.EndPage = endPage;
            return PartialView("ListViewComponents/_Paging", pagingViewModel);
        }
    }
}