﻿using LuandryCustomerApp.Infrastructure;
using LuandryCustomerApp.Models;
using LuandryCustomerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LuandryCustomerApp.Controllers
{
    public class PlacedOrdersController : Controller
    {
        private ModelMapping modelMapping = new ModelMapping();
        private lddsDBEntities dbContext = new lddsDBEntities();
        //private AppDbContext dbContext = new AppDbContext();
        // GET: PlacedOrders
        public ActionResult Index(string sortField = "id_asc", int pageNumber = 1, int pageSize = 10)
        {
            IQueryable<placed_orders> query0 = dbContext.placed_orders;

            var query = query0.Select(x => new PlacedOrderListViewModel()
            {
                id = x.id,
                created_at = x.created_at,
                updated_at = x.updated_at,
                deleted_at = x.deleted_at,
                store_id = x.store_id,
                time_placed = x.time_placed,
                detail = x.detail,
                order_status_id = (OrderStatus)x.order_status_id,
                customer_id = x.customer_id,
                capacity = x.capacity,
                estimated_capacity = x.estimated_capacity,
                delivery_address = x.delivery_address,
                delivery_latitude = x.delivery_latitude,
                delivery_longitude = x.delivery_longitude,
                service_total_price = x.service_total_price,
                priority = x.priority,
                review_id = x.review_id
            });

            int totalPages = (int)Math.Ceiling((decimal)query.Count() / pageSize);

            query = query.OrderBySortField(sortField);
            query = query.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            ViewBag.TotalPages = totalPages;
            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = pageSize;
            return View(query.ToList());
        }

        [HttpPost]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                placed_orders placedOrder = dbContext.placed_orders.Find(id);
                if (placedOrder == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound, $"placed_orders {id} is not found.");

                dbContext.placed_orders.Remove(placedOrder);
                dbContext.SaveChanges();

                if (Request.IsAjaxRequest())
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK, "Delete Success!");
                }
                else
                {
                    return RedirectToAction("Index", "PlacedOrders", new RouteValueDictionary(
                        this.ParseQueryString(Request.Form["RouteValues"] as string, this.CalculatePageNumber)));
                }
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }


        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateByGet()
        {
            ViewBag.CustomerData = GetCustomerData();
            ViewBag.StoreData = GetStoreData();
            return View();
        }

        [HttpPost]
        [ActionName("Create")]
        public ActionResult CreateByPost(PlacedOrderModifyViewModel viewModel)
        {
            
            if (ModelState.IsValid)
            {
                placed_orders placedOrder = modelMapping.CreatePlacedOrderModel(viewModel);
                dbContext.placed_orders.Add(placedOrder);
                dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerData = GetCustomerData();
            ViewBag.StoreData = GetStoreData();
            return View(viewModel);
        }

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult EditByGet(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            placed_orders placedOrder = dbContext.placed_orders.Find(id);
            if (placedOrder == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            PlacedOrderModifyViewModel viewModel = modelMapping.CreatePlacedOrderModifyModel(placedOrder);
            ViewBag.CustomerData = GetCustomerData();
            ViewBag.StoreData = GetStoreData();
            return View(viewModel);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditByPost(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            placed_orders placedOrder = dbContext.placed_orders.Find(id);
            if (placedOrder == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            PlacedOrderModifyViewModel viewModel = new PlacedOrderModifyViewModel();

            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                modelMapping.UpdatePlacedOrderModel(viewModel, placedOrder);
                dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerData = GetCustomerData();
            ViewBag.StoreData = GetStoreData();
            return View(viewModel);
        }

        private List<SelectListItem> GetCustomerData()
        {
            return dbContext.customers.Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id.ToString()
            }).ToList();
        }

        private List<SelectListItem> GetStoreData()
        {
            return dbContext.stores.Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id.ToString()
            }).ToList();
        }

       



        public ActionResult Test()
        {
            return View();
        }
    }
}