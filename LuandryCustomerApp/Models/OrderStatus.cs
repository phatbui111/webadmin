﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace LuandryCustomerApp.Models
{
    public enum OrderStatus
    {
        [Description("Order just Created")]
        OrderjustCreated = 1,

        [Description("Accepted orders")]
        AcceptedOrders = 2,

        [Description("Delivery verified orders")]
        DeliveryVerifiedOrders = 3,

        [Description("Pick up order to warehouse")]
        PickUpOrderWarehouse = 4,

        [Description("Store xx received order")]
        StoreReceivedOrder = 5,

        [Description("Order is in process")]
        OrderIsInProcess = 6,

        [Description("Order is finished laundry")]
        OrderIsFinishedLaundry = 7,

        [Description("Order is delivering")]
        OrderIsDelivering = 8,

        [Description("Order completed")]
        OrderCompleted = 9,

        
    }
}