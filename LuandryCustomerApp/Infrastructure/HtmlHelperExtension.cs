﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace LuandryCustomerApp.Infrastructure
{
    public static class HtmlHelperExtension
    {
        public static string BuildQueryString(this HtmlHelper htmlHelper, RouteValueDictionary routeValues, string[] excludeKeys = null)
        {
            excludeKeys?.ToList().ForEach(x =>
            {
                if (routeValues.ContainsKey(x)) routeValues.Remove(x);
            });

            return routeValues
                .Where(x => !string.IsNullOrEmpty(Convert.ToString(x.Value)))
                .Aggregate(string.Empty, (a, b) => $"{a}&{b.Key}={b.Value}", r => r.Length > 0 && r.ElementAt(0) == '&' ? r.Substring(1) : r);
        }

        public static string CombineQueryString(this HtmlHelper htmlHelper, string defaultQueryString, Dictionary<string, object> includeKeyValuePairs = null)
        {
            return includeKeyValuePairs?
                .Where(x => !string.IsNullOrEmpty(Convert.ToString(x.Value)))
                .Aggregate(defaultQueryString ?? string.Empty, (a, b) => $"{a}&{b.Key}={b.Value}", r => r.Length > 0 && r.ElementAt(0) == '&' ? r.Substring(1) : r);
        }

        public static MvcHtmlString BuildSortFieldFor<TModel, TValue>(this HtmlHelper<IEnumerable<TModel>> html,
            Expression<Func<TModel, TValue>> expression, string sortField, string actionName, string controllerName,
            RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            var me = expression.Body as MemberExpression;
            var propName = me.Member.Name;
            routeValues["SortField"] = $"{propName}_{(sortField == $"{propName}_asc" ? "desc" : "asc")}";
            return html.ActionLink(html.DisplayNameFor(expression).ToString(), actionName, controllerName, routeValues, htmlAttributes);
        }
    }
}