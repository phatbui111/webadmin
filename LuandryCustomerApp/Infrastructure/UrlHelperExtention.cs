﻿using LuandryCustomerApp.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LuandryCustomerApp.Infrastructure
{
    public static class UrlHelperExtention
    {
        public static string AppAssetsPath(this UrlHelper url, string source)
        {
            return url.Content($"{AppConfig.App_Assets_Path}{source}");

        }
    }
}