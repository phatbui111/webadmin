﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LuandryCustomerApp.Infrastructure
{
    public static class ControllerExtension
    {
        public static Dictionary<string, object> ParseQueryString(this Controller controller, string queryString, Action<Dictionary<string, object>> resultSelector = null)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();

            queryString?.Split('&').Aggregate(dictionary, (dict, s) =>
            {
                string[] segments = s.Split('=');
                if (segments.Count() == 2)
                {
                    dict.Add(segments[0], segments[1]);
                }
                return dict;
            });

            resultSelector?.Invoke(dictionary);
            return dictionary;
        }

        public static void CalculatePageNumber(this Controller controller, Dictionary<string, object> routeValues)
        {
            int pageNumber = Convert.ToInt32(routeValues?["PageNumber"] ?? 1);
            int numberOfRows = Convert.ToInt32(routeValues?["NumberOfRows"]);
            routeValues["PageNumber"] = numberOfRows == 1 && pageNumber > 1 ? pageNumber - 1 : pageNumber;
        }
    }
}