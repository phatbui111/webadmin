﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace LuandryCustomerApp.Infrastructure
{
    public static class LinqExtension
    {
        public static IQueryable<T> OrderBySortField<T>(this IQueryable<T> q, string sortField)
        {
            //FullName_asc
            string[] segments = sortField.Split('_');//[FullName, asc]
            var pe = Expression.Parameter(typeof(T), "x");
            var me = Expression.Property(pe, segments[0]);
            var le = Expression.Lambda(me, pe);
            q = (IOrderedQueryable<T>)q
                .Provider
                .CreateQuery(Expression.Call(typeof(Queryable), segments[1] == "asc" ? "OrderBy" : "OrderByDescending", new[] { typeof(T), me.Type }, q.Expression, le));
            return q;
        }
    }
}