﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace LuandryCustomerApp.App_Start
{
    public class AppConfig
    {
        public static readonly string App_Assets_Path = WebConfigurationManager.AppSettings["App.Assets.Path"];
    }
}