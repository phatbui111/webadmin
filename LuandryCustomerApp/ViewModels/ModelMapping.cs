﻿using LuandryCustomerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuandryCustomerApp.ViewModels
{
    public class ModelMapping
    {
        public placed_orders CreatePlacedOrderModel(PlacedOrderModifyViewModel viewModel)
        {
            return new placed_orders()
            {
                id = viewModel.id,
                created_at = DateTime.Now,
                updated_at = viewModel.updated_at,
                deleted_at = viewModel.deleted_at,
                store_id = viewModel.store_id,
                time_placed = viewModel.time_placed,
                detail = viewModel.detail,
                order_status_id = (long?)viewModel.order_status_id,
                customer_id = viewModel.customer_id,
                capacity = viewModel.capacity,
                estimated_capacity = viewModel.estimated_capacity,
                delivery_address = viewModel.delivery_address,
                delivery_latitude = viewModel.delivery_latitude,
                delivery_longitude = viewModel.delivery_longitude,
                service_total_price = viewModel.service_total_price,
                priority = viewModel.priority,
                review_id = viewModel.review_id           
            };
        }

        public PlacedOrderModifyViewModel CreatePlacedOrderModifyModel(placed_orders model)
        {
            return new PlacedOrderModifyViewModel()
            {
                id = model.id,
                created_at = model.created_at,
                updated_at = model.updated_at,
                deleted_at = model.deleted_at,
                store_id = model.store_id,
                time_placed = model.time_placed,
                detail = model.detail,
                order_status_id = (OrderStatus)model.order_status_id,
                customer_id = model.customer_id,
                capacity = model.capacity,
                estimated_capacity = model.estimated_capacity,
                delivery_address = model.delivery_address,
                delivery_latitude = model.delivery_latitude,
                delivery_longitude = model.delivery_longitude,
                service_total_price = model.service_total_price,
                priority = model.priority,
                review_id = model.review_id
            };
        }
        public void UpdatePlacedOrderModel(PlacedOrderModifyViewModel viewModel, placed_orders model)
        {
            model.id = viewModel.id;
            model.created_at = viewModel.created_at;
            model.updated_at = DateTime.Now;
            model.deleted_at = viewModel.deleted_at;
            model.store_id = viewModel.store_id;
            model.time_placed = viewModel.time_placed;
            model.detail = viewModel.detail;
            model.order_status_id = (long?)viewModel.order_status_id;
            model.customer_id = viewModel.customer_id;
            model.capacity = viewModel.capacity;
            model.delivery_address = viewModel.delivery_address;
            model.estimated_capacity = viewModel.estimated_capacity;
            model.delivery_address = viewModel.delivery_address;
            model.delivery_latitude = viewModel.delivery_latitude;
            model.delivery_longitude = viewModel.delivery_longitude;
            model.service_total_price = viewModel.service_total_price;
            model.priority = viewModel.priority;
            model.review_id = viewModel.review_id;        
        }
    }
}