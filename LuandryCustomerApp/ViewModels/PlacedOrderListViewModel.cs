﻿using LuandryCustomerApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LuandryCustomerApp.ViewModels
{
    public class PlacedOrderListViewModel
    {
        public long id { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "Create At")]
        public DateTime? created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "Updated At")]
        public DateTime? updated_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "Deleted At")]
        public DateTime? deleted_at { get; set; }

        [Display(Name = "Store Id")]
        public long? store_id { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "Time Placed")]
        public DateTime? time_placed { get; set; }

        [Display(Name = "Detail")]
        public string detail { get; set; }

        [Display(Name = "Status")]
        public OrderStatus order_status_id { get; set; }

        [Display(Name = "Customer Name")]
        public long? customer_id { get; set; }

        [Display(Name = "Capacity(kg)")]
        public double? capacity { get; set; }

        [Display(Name = "Estimated Capacity(kg)")]
        public double? estimated_capacity { get; set; }

        [Display(Name = "Delivery Address")]
        public string delivery_address { get; set; }

        [Display(Name = "Delivery Latitude")]
        public double? delivery_latitude { get; set; }

        [Display(Name = "Delivery Longitude")]
        public double? delivery_longitude { get; set; }

        [Display(Name = "Total Price(VND)")]
        public double? service_total_price { get; set; }
        public long? priority { get; set; }

        [Display(Name = "Review Id")]
        public long? review_id { get; set; }

    }
}