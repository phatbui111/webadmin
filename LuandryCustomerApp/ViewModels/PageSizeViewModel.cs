﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace LuandryCustomerApp.ViewModels
{
    public class PageSizeViewModel
    {
        public int SelectedValue { get; set; } = 10;
        public RouteValueDictionary RouteValues { get; set; }
    }
}