﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;


namespace LuandryCustomerApp.ViewModels
{
    public class PagingViewModel
    {
        public string Action { get; set; }
        public string Controller { get; set; }
        public int StartPage { get; set; } = 1;
        public int EndPage { get; set; }
        public int PageNumber { get; set; }
        public int TotalPages { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
    }
}