﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuandryCustomerApp.ViewModels
{
    public class CustomerListViewModel
    {
        public long id { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> deleted_at { get; set; }
        public string name { get; set; }
        public string shipping_address { get; set; }
        public Nullable<double> longitude { get; set; }
        public Nullable<double> latitude { get; set; }
        public string avatar_url { get; set; }
        public Nullable<long> account_id { get; set; }
    }
}