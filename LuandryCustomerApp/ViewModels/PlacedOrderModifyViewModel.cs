﻿using LuandryCustomerApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LuandryCustomerApp.ViewModels
{
    public class PlacedOrderModifyViewModel
    {

        public long id { get; set; }

        [DisplayFormat(DataFormatString ="0:MM/dd/yyyy")]
        public DateTime? created_at { get; set; }

        [DisplayFormat(DataFormatString = "0:MM/dd/yyyy")]
        public DateTime? updated_at { get; set; }

        [DisplayFormat(DataFormatString = "0:MM/dd/yyyy")]
        public DateTime? deleted_at { get; set; }

        [Required]
        public long? store_id { get; set; }

        [DisplayFormat(DataFormatString = "0:MM/dd/yyyy")]

        [Required]
        public DateTime? time_placed { get; set; }


        public string detail { get; set; }


        public OrderStatus order_status_id { get; set; }


        public long? customer_id { get; set; }

        [Required]
        public double? capacity { get; set; }
        [Required]
        public double? estimated_capacity { get; set; }
        [Required]
        public string delivery_address { get; set; }

        public double? delivery_latitude { get; set; }

        public double? delivery_longitude { get; set; }

        public double? service_total_price { get; set; }
        public int? priority { get; set; }

        public long? review_id { get; set; }
    }
}


